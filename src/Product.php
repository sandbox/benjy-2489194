<?php

/**
 * @file
 * Contains \Drupal\commerce_oo\Product
 */

namespace Drupal\commerce_oo;

class Product {

  /**
   * The stdClass representing the Commerce product object.
   *
   * @var object
   */
  protected $product;

  /**
   * The metadata wrapper to make it easier to work with this object.
   *
   * @var \EntityMetadataWrapper
   */
  protected $productWrapper;

  /**
   * Constructs a new product object.
   *
   * @param int|object $product
   *   A Commerce product object or a valid product_id.
   */
  public function __construct($product) {
    if (is_int($product)) {
      $product = commerce_product_load($product);
    }
    $this->product = $product;
  }

  /**
   * Gets the unique line item id.
   *
   * @return int
   *   The unique line item id.
   */
  public function id() {
    return (int) $this->product->product_id;
  }

  /**
   * Gets the product label.
   *
   * @return string
   *   The product title.
   */
  public function label() {
    return $this->product->title;
  }

  /**
   * Gets the product sku.
   *
   * @return string
   *   The product Sku.
   */
  public function getSku() {
    return $this->product->sku;
  }

  /**
   * Gets the product type.
   *
   * @return string
   *   The product type.
   */
  public function getType() {
    return $this->product->type;
  }

  /**
   * Gets the price information for this product.
   *
   * @return array
   *   A Commerce amount array.
   */
  public function getPrice() {
    return $this->getWrapper()->commerce_price->value();
  }

  /**
   * Gets the wrapper for this object.
   *
   * @return \EntityMetadataWrapper
   *   The EMW to make loading references easier.
   */
  public function getWrapper() {
    if (!isset($this->productWrapper)) {
      $this->productWrapper = entity_metadata_wrapper('commerce_product', $this->product);
    }
    return $this->productWrapper;
  }

  /**
   * Gets the raw Commerce object.
   *
   * @return \stdClass
   */
  public function getObject() {
    return $this->product;
  }

}
