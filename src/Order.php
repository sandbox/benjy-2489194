<?php

/**
 * @file
 * Contains \Drupal\commerce_oo\Order
 */

namespace Drupal\commerce_oo;

use Drupal\commerce_oo\Iterator\MapIterator;

class Order {

  /**
   * The stdClass representing the Commerce order object.
   *
   * @var object
   */
  protected $order;

  /**
   * The metadata wrapper to make it easier to work with this object.
   *
   * @var \EntityMetadataWrapper
   */
  protected $orderWrapper;

  /**
   * Constructs a new Order object.
   *
   * @param int|object $order
   *   A Commerce order object or a valid order_id.
   */
  public function __construct($order) {
    if (is_int($order)) {
      $order = commerce_order_load($order);
    }
    $this->order = $order;
  }

  /**
   * Get the unique order for this id.
   *
   * @return int
   *   The unique order id.
   */
  public function id() {
    return (int) $this->order->order_id;
  }

  /**
   * Gets an
   * @return \Drupal\commerce_oo\LineItem[]
   *   An array of line items.
   */
  public function getLineItems() {
    return new MapIterator($this->getWrapper()->commerce_line_items->value(), function($line_item) {
      return new LineItem($line_item);
    });
  }

  /**
   * Update our internal order state. Needed when when deleting line items.
   *
   * @param object $order
   *   The commerce order object.
   */
  protected function updateOrder($order) {
    $this->order = $order;
    unset($this->orderWrapper);
  }

  /**
   * Persist the order.
   */
  public function save() {
    commerce_order_save($this->order);
  }

  /**
   * Check if the order has been checked out.
   *
   * @return bool
   *   TRUE if this order has been paid for otherwise FALSE.
   */
  public function hasCheckedOut() {
    return in_array($this->order->status, [
      'pending',
      'completed',
      'canceled-credit',
      'canceled-noshow',
      'canceled-manual'
    ]);
  }

  /**
   * Gets the order wrapper for this object.
   *
   * @return \EntityMetadataWrapper
   *   The EMW to make loading references easier.
   */
  public function getWrapper() {
    if (!isset($this->orderWrapper)) {
      $this->orderWrapper = entity_metadata_wrapper('commerce_order', $this->order);
    }
    return $this->orderWrapper;
  }

  /**
   * Gest the raw order object.
   *
   * @return \stdClass
   *   The Commerce order object.
   */
  public function getObject() {
    return $this->order;
  }

  /**
   * Removes a line item from the this order.
   *
   * @param \Drupal\commerce_oo\LineItem $lineItem
   *   The line item to remove.
   */
  public function removeLineItem(LineItem $lineItem) {
    $order = commerce_cart_order_product_line_item_delete($this->order, $lineItem->id());
    $this->updateOrder($order);
  }

  /**
   * Add a line item to this order.
   *
   * @param int $product_id
   *   The product id to add.
   * @param string $line_item_type
   *   The line item type.
   *
   * @return $this;
   */
  public function addLineItem($product_id, $line_item_type = 'product') {
    $product = commerce_product_load($product_id);
    $line_item = commerce_product_line_item_new($product, 1, $this->order->order_id, array(), $line_item_type);
    commerce_line_item_save($line_item);
    $this->getWrapper()->commerce_line_items[] = $line_item;

    return $this;
  }

}

