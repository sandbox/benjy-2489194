<?php

namespace Drupal\commerce_oo;

/**
 * @file
 * Contains \Drupal\commerce_oo\LineItem
 */

class LineItem {

  /**
   * The stdClass representing the Commerce line item object.
   *
   * @var object
   */
  protected $lineItem;

  /**
   * The metadata wrapper to make it easier to work with this object.
   *
   * @var \EntityMetadataWrapper
   */
  protected $lineItemWrapper;

  /**
   * Constructs a new line item object.
   *
   * @param int|object $line_item
   *   A Commerce line item object or a valid line_item_id.
   */
  public function __construct($line_item) {
    if (is_int($line_item)) {
      $line_item = commerce_line_item_load($line_item);
    }
    $this->lineItem = $line_item;
  }

  /**
   * Gets the unique line item id.
   *
   * @return int
   *   The unique line item id.
   */
  public function id() {
    return (int) $this->lineItem->line_item_id;
  }

  /**
   * Gets the Commerce amount array representing the total for this line item.
   *
   * @return array
   *   A Commerce amount array.
   */
  public function getTotal() {
    return $this->getWrapper()->commerce_total->value();
  }

  /**
   * Gets the Commerce amount array representing the unit price for the product.
   *
   * @return array
   *   A Commerce amount array.
   */
  public function getUnitPrice() {
    return $this->getWrapper()->commerce_unit_price->value();
  }

  /**
   * Gets the commerce product object.
   *
   * @return \Drupal\commerce_oo\Product
   *   The product object.
   */
  public function getProduct() {
    return new Product($this->getWrapper()->commerce_product->value());
  }

  /**
   * Gets the line item type.
   *
   * @return string
   *   The line item type.
   */
  public function getType() {
    return $this->lineItem->type;
  }

  /**
   * Gets the line item wrapper for this object.
   *
   * @return \EntityMetadataWrapper
   *   The EMW to make loading references easier.
   */
  public function getWrapper() {
    if (!isset($this->lineItemWrapper)) {
      $this->lineItemWrapper = entity_metadata_wrapper('commerce_line_item', $this->lineItem);
    }
    return $this->lineItemWrapper;
  }

  /**
   * Gets the raw Commerce line item object.
   *
   * @return \stdClass
   */
  public function getObject() {
    return $this->lineItem;
  }

}
